package org.app;

public class QueueMainTwo {

	public static void main(String[] args) {

		QueueDymaic q = new QueueDymaic();
		q.enqueue(3);
		q.enqueue(4);
		q.enqueue(5);
		q.enqueue(5);
		q.dequeue();
		q.dequeue();
		q.dequeue();
		q.dequeue();
		q.dequeue();
		q.display();

	}
}

class Node {

	int data;
	Node next;
}

class QueueDymaic {
	Node front = null;
	Node rear = null;

	public void enqueue(int x) {
		Node newNode = new Node();
		newNode.data = x;

		if (front == null && rear == null) {
			front = rear = newNode;
		} else {
			rear.next = newNode;
			rear = newNode;
		}
	}

	public void display() {
		Node temp = front;
		if (front == null && rear == null) {
			System.out.println("underload queue");
		} else {
			while (temp != null) {
				System.out.println(temp.data);
				temp = temp.next;
			}
		}
	}

	public void dequeue() {
		Node temp;

		if (front == null && rear == null) {
			System.out.println("queue is empty");
		} else if (front == rear) {
			System.out.println(front.data);
			front = rear = null;
		}

		else {
			System.out.println(front.data);
			temp = front.next;
			front.next = null;

			front = temp;
		}
	}
}
